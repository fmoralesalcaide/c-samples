#pragma once

namespace Trees
{

template<class T>
class BinaryTreeNode
{
public:
    T key;
    unsigned int count;
    BinaryTreeNode* parent;
    BinaryTreeNode* left;
    BinaryTreeNode* right;
    BinaryTreeNode(const T& _key) :
        key(_key), count(1), parent(nullptr), left(nullptr), right(nullptr)
    {
    }
    ;
    virtual ~BinaryTreeNode()
    {
    }
    ;
    bool isLeaf()
    {
        return left == nullptr && right == nullptr;
    }
};

template<class T>
class BinaryTree
{
protected:
    Trees::BinaryTreeNode* root;
    unsigned int numElements;
public:
    BinaryTree() :
        root(nullptr), numElements(0)
    {
    }
    ;
    virtual ~BinaryTree()
    {
        // In-depth destruction
        Trees::BinaryTreeNode* current = root;
        while(current != nullptr)
        {
            if(current->left != nullptr)
            {
                current = current->left;
            }
            else if(current->right != nullptr)
            {
                current = current->right;
            }
            else
            {
                BinaryTreeNode* parent = current->parent;
                delete(current);
                current = parent;
            }
        }
    }
    ;

    virtual void insert(const T& newKey) = 0;
    virtual void remove(const T& key)    = 0;
    virtual bool find(const T& newKey)   = 0;
    unsigned int size()
    {
        return numElements;
    };
};

template<class T>
class BinarySearchTree: Trees::BinaryTree
{
public:
    BinarySearchTree()
    {
    }
    ;
    virtual ~BinarySearchTree()
    {
    }
    ;

    virtual void insert(const T& newKey)
    {
        ++numElements;
        Trees::BinaryTreeNode* newNode = new BinaryTreeNode(newKey);
        if(root == nullptr)
        {
            newNode->parent = nullptr;
            return;
        }
        Trees::BinaryTreeNode* current = root;
        Trees::BinaryTreeNode* previous = nullptr;
        while (current != nullptr)
        {
            previous = current;
            if (current->key == newKey)
            {
                ++current->count;
                return;
            }
            else if (newKey < current->key)
                current = current->left;
            else if (newKey > current->key)
                current = current->right;
        }
        newNode->parent = previous;
        if(newKey < previous->key)
            previous->left = newNode;
        else
            previous->right = newNode;
    }
    virtual void remove(const T& newKey)
    {
        Trees::BinaryTreeNode* current = root;
        while (current != nullptr)
        {
            if (current->key == newKey)
            {
                if(current->count > 1)
                    --current->count;
                else
                {
                    // Basic deletion
                    if(current->isLeaf())
                    {
                        Trees::BinaryTreeNode* parent = current->parent;
                        if(parent->right == current)
                            parent->right = nullptr;
                        else
                            parent->left = nullptr;
                        delete(current);
                    }
                    else
                    {
                        if(current->right == nullptr)
                        {
                            current->left->parent = current->parent;
                            current->parent->left = current->left;
                            delete(current);
                        }
                        else if(current->left == nullptr)
                        {
                            current->right->parent = current->parent;
                            current->parent->right = current->right;
                            delete(current);
                        }
                        else
                        {
                            // TODO delete with two children.

                        }

                    }
                }
                --numElements;
                return;
            }
            else if (newKey < current->key)
                current = current->left;
            else if (newKey > current->key)
                current = current->right;
        }
    }
    virtual bool find(const T& newKey)
    {
        Trees::BinaryTreeNode* current = root;
        while (current != nullptr)
        {
            if (current->key == newKey)
                return true;
            else if (newKey < current->key)
                current = current->left;
            else if (newKey > current->key)
                current = current->right;
        }
        return false;
    }
};

}

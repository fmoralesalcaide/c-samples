#pragma once

#include <vector>
#include <cstdlib>

template <class T>
class HashFunction
{
private:
    long a_, b_;
    int M_;
public:
    HashFunction(int M)
    {
        a_ = rand();
        b_ = rand();
        M_ = M;
    }
    int operator ()(const T& x) const
    {
        // Jenkins' "one-at-a-time" hash function.
        unsigned char* bytes = ((unsigned char*) (&x));
        unsigned int hash, i;
        for (hash = i = 0; i < sizeof(x); ++i)
        {
            hash += bytes[i];
            hash += (hash << 10);
            hash ^= (hash >> 6);
        }
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);
        return (a_ * hash + b_) % M_;
    }
};

template <class T>
class BloomFilter
{
private:
    std::vector<bool> F;
    std::vector<HashFunction<T>> h;
public:
    BloomFilter(int M, int k)
    {
        F = std::vector<bool>(M, false);
        for (int i = 0; i < k; ++i)
            h.push_back(HashFunction<T>(M));
    }
    void insert(const T& x)
    {
        for(unsigned int i = 0; i < h.size(); ++i)
            F[h[i](x)] = true;
    }
    bool find(const T& x)
    {
        for(unsigned int i = 0; i < h.size(); ++i)
            if(F[h[i](x)] == false)
                return false;
        return true;
    }
};


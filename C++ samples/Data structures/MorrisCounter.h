#pragma once

#include <ctime>
#include <cmath>
#include <random>

class MorrisCounter
{
private:
    long double base, x, d;
    unsigned int counted;
    std::default_random_engine generator;
    std::uniform_real_distribution<long double> distribution;
    void feed()
    {
        x = distribution(generator);
        d = pow(base, counted);
        if(x*d <= 1)
            ++counted;
    }
public:
    MorrisCounter(long double base, int seed = time(NULL)):
        base(base),
        x(0),
        d(0),
        counted(0),
        generator(seed),
        distribution(0.0,1.0)
    {}
    virtual ~MorrisCounter()
    {}

    void reset()
    {
        counted = 0;
    }
    void feed(unsigned int n)
    {
        for(unsigned int i=0; i<n; ++i)
            feed();
    }
    unsigned int count()
    {
        return pow(base, counted) - base;
    }
};

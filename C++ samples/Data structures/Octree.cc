#include <Pot/Managers/CollisionManager/Octree.h>

namespace Pot
{

Octree::Node::Node()
	: parent(nullptr),
	  pos(0,0,0),
	  radius(OCTREE_DEFAULT_MINRADIUS),
	  collidersBelow(0)
{
	for(unsigned int i = 0; i < 8; ++i) children[i] = nullptr;
}

Octree::Node::~Node()
{
	for(int i = 0; i < 8; ++i)
	{
		if(children[i]) delete children[i];
	}
}

Octree::Octree(unsigned int minDensity, unsigned int maxDensity, bool resizable)
	: minDensity(minDensity),
	  maxDensity(maxDensity),
	  resizable(resizable),
	  root(new Node())
{
	if(minDensity > maxDensity)
		THROW_EXCEPTION("The minDensity value cannot be greater than the maxDensity value");
	if(minDensity == maxDensity)
		THROW_EXCEPTION("The minDensity value cannot be equal to the maxDensity value");
}

Octree::~Octree()
{
	delete root;
}

void Octree::insert(Components::Collider* col, Octree::Node* node)
{
	if(!col)
		THROW_EXCEPTION("Cannot insert a null collider in the octree");

	// Not contained in the octree
	if(!isWithinOctet(col, root))
	{
		outliers[col->getId()] = col;
	}
	// Contained in the octree
	else
	{
		node = node ? node : root;
		glm::vec3 pos = col->getWorldPosition();

		while(!node->isLeaf())
		{
			node->collidersBelow += 1;
			node = node->children[getChildIndex(node->pos, pos)];
		}

		node->colliders[col->getId()] = col;
		containingNode[col->getId()] = node;

		checkExpand(node);
	}
}

void Octree::remove(Components::Collider* col)
{
	bool contained;
	Octree::Node* node;
	std::tie(contained, node) = getNode(col);

	// Not contained in the octree
	if(!contained)
	{
		outliers.erase(col->getId());
	}
	// Contained in the octree
	else
	{
		containingNode.erase(col->getId());
		node->colliders.erase(col->getId());

		Octree::Node* parent = node->parent;

		while(parent)
		{
			parent->collidersBelow -= 1;
			parent = parent->parent;
		}

		checkContract(node);
	}
}

void Octree::update(Components::Collider* col)
{
	remove(col);
	insert(col);
}

void Octree::postUpdates()
{
	if(!resizable)
		return;

	checkOutliers();
	checkRoot();
}

std::pair<bool, Octree::Node*> Octree::getNode(Components::Collider* col)
{
	if(outliers.count(col->getId()))
		return std::make_pair(false, nullptr);

	if(!containingNode.count(col->getId()))
		THROW_EXCEPTION("Collider " << col->getId() << " not stored in octree");

	return std::make_pair(true, containingNode.at(col->getId()));
}

void Octree::render(Octree::Node* node)
{
	node = node ? node : root;

	// Draw parent element
	double x = node->pos.x;
	double y = node->pos.y;
	double z = node->pos.z;
	double r = node->radius;

	glLineWidth(1.0f);
	glBegin(GL_LINE_LOOP);
		glVertex3f(x-r,y+r,z+r);
		glVertex3f(x-r,y-r,z+r);
		glVertex3f(x+r,y-r,z+r);
		glVertex3f(x+r,y+r,z+r);
	glEnd();
	glBegin(GL_LINE_LOOP);
		glVertex3f(x-r,y+r,z-r);
		glVertex3f(x-r,y-r,z-r);
		glVertex3f(x+r,y-r,z-r);
		glVertex3f(x+r,y+r,z-r);
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(x-r,y-r,z-r);
		glVertex3f(x-r,y-r,z+r);
		glVertex3f(x+r,y-r,z-r);
		glVertex3f(x+r,y-r,z+r);
		glVertex3f(x-r,y+r,z-r);
		glVertex3f(x-r,y+r,z+r);
		glVertex3f(x+r,y+r,z-r);
		glVertex3f(x+r,y+r,z+r);
	glEnd();

	// Call render in children, if any
	for(unsigned int i = 0; i < 8; ++i)
	{
		if(node->children[i])
			render(node->children[i]);
	}
}

unsigned int Octree::getChildIndex(const glm::vec3& center, const glm::vec3& pos)
{
	int idx1 = (pos.x - center.x < 0);
	int idx2 = (pos.y - center.y < 0);
	int idx3 = (pos.z - center.z < 0);
	return idx1 + 2 * idx2 + 4 * idx3;
}

glm::vec3 Octree::getChildDirection(unsigned int i)
{
	glm::vec3 v;
	v.x = (i & 1) ? -1 : 1;
	v.y = (i & 2) ? -1 : 1;
	v.z = (i & 4) ? -1 : 1;
	return v;
}

void Octree::checkExpand(Octree::Node* node)
{
	if(node->colliders.size() <= maxDensity)
		return;
	if(node->radius <= 1)
		return;

	// Create eight child nodes
	for(unsigned int i = 0; i < 8; ++i)
	{
		Octree::Node* child = new Octree::Node();
		node->children[i] = child;
		child->parent = node;
		child->radius = node->radius / 2;
		child->pos = node->pos + float(child->radius) * getChildDirection(i);
	}

	auto nodeColliders = node->colliders;
	node->colliders.clear();

	// Re-distribute elements in the new nodes
	for(auto keyVal : nodeColliders)
	{
		Components::Collider* collider = keyVal.second;
		containingNode.erase(collider->getId());
		insert(collider, node);
	}
}

void Octree::checkContract(Octree::Node* node)
{
	if(!node->parent)
		return;
	if(node->parent->collidersBelow >= minDensity)
		return;

	// Contract the eight siblings into the parent
	Octree::Node* parent = node->parent;

	for(unsigned int i = 0; i < 8; ++i)
	{
		for(auto keyVal : parent->children[i]->colliders)
		{
			Components::Collider* collider = keyVal.second;
			containingNode.at(collider->getId()) = parent;
			parent->colliders[collider->getId()] = collider;
		}

		delete parent->children[i];
		parent->children[i] = nullptr;
	}

	parent->collidersBelow = 0;
	checkContract(parent);
}

void Octree::checkOutliers()
{
	double misses = outliers.size();
	double total = misses + root->getNumCollidersLEQ();
	double missRatio = total ? misses / total : 0;

	if(missRatio <= 0.5)
		return;

	// Crate root one level above, centered towards outliers
	glm::vec3 centroid = computeCentroid(outliers);
	unsigned int idx = getChildIndex(centroid, root->pos);
	glm::vec3 diagonal = -getChildDirection(idx);

	Octree::Node* newRoot = new Octree::Node();
	newRoot->radius = root->radius * 2;
	newRoot->pos = root->pos + float(root->radius) * diagonal;
	newRoot->collidersBelow = root->getNumCollidersLEQ();
	newRoot->parent = nullptr;

	for(unsigned int i = 0; i < 8; ++i)
	{
		if(i == idx)
		{
			newRoot->children[i] = root;
			root->parent = newRoot;
		}
		else
		{
			Octree::Node* child = new Octree::Node();
			child->radius = root->radius;
			child->pos = newRoot->pos + float(child->radius) * getChildDirection(i);
			child->parent = newRoot;
			newRoot->children[i] = child;
		}
	}

	root = newRoot;

	// Insert outliers in the bigger octree
	auto outliersAux = outliers;
	outliers.clear();

	for(auto keyVal : outliersAux)
	{
		Components::Collider* col = keyVal.second;
		insert(col);
	}
}

void Octree::checkRoot()
{
	// Do not shrink the octree root if it is below a given radius
	if(root->radius <= OCTREE_DEFAULT_MINRADIUS)
		return;

	// Resize a (leaf) root node if it is sparse
	if(root->isLeaf())
	{
		std::unordered_set<unsigned int> usedOcts;

		for(auto keyVal : root->colliders)
		{
			Components::Collider* col = keyVal.second;
			usedOcts.insert(getChildIndex(root->pos, col->getWorldPosition()));
		}

		if(usedOcts.size() == 1)
		{
			unsigned int idx = *usedOcts.begin();
			root->radius /= 2;
			root->pos += float(root->radius) * getChildDirection(idx);
		}
	}
	// Replace a (non-leaf) root node if it is sparse, using one of its children as new root
	else
	{
		unsigned int usedChildren = 0;
		unsigned int idx = 0;

		for(unsigned int i = 0; i < 8; ++i)
		{
			if(root->children[i] && root->children[i]->getNumCollidersLEQ())
			{
				++usedChildren;
				idx = i;
			}
		}

		if(usedChildren == 1)
		{
			// Transform only used child in new root
			Octree::Node* oldParent = root;
			root = oldParent->children[idx];
			root->parent = nullptr;

			oldParent->children[idx] = nullptr;
			delete oldParent;
		}
	}
}

bool Octree::isWithinOctet(Components::Collider* col, Octree::Node* node)
{
	glm::vec3 pos = col->getWorldPosition();
	double u = glm::compMax(glm::abs(pos - node->pos));
	return u <= node->radius;
}

glm::vec3 Octree::computeCentroid(const std::map<uint32_t, Components::Collider*>& colliders)
{
	glm::vec3 centroid(0);

	for(auto keyVal : colliders)
	{
		Components::Collider* col = keyVal.second;
		centroid += col->getWorldPosition();
	}

	centroid /= double(colliders.size());
	return centroid;
}

}

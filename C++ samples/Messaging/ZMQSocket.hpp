#pragma once

#include <thread>
#include <zmq.h>

#include "../Messaging/Request.hpp"

using namespace std;

class ZMQSocket
{
  public:
    /*! Constructor */
    static ZMQSocket& Instance ();
    virtual ~ZMQSocket();

    typedef enum
    {
      _ZMQ_EVENT_CONNECTED_         = 1,
      _ZMQ_EVENT_CONNECT_DELAYED_   = 2,
      _ZMQ_EVENT_CONNECT_RETRIED_   = 4,
      _ZMQ_EVENT_LISTENING_         = 8,
      _ZMQ_EVENT_BIND_FAILED_       = 16,
      _ZMQ_EVENT_ACCEPTED_          = 32,
      _ZMQ_EVENT_ACCEPT_FAILED_     = 64,
      _ZMQ_EVENT_CLOSED_            = 128,
      _ZMQ_EVENT_CLOSE_FAILED_      = 256,
      _ZMQ_EVENT_DISCONNECTED_      = 512,
      _ZMQ_EVENT_MONITOR_STOPPED_   = 1024
    } Event;

    typedef enum
    {
      _SUCCESS_,
      _BUSY_,
      _WRONG_FORMAT_,
      _WRONG_SIZE_
    } Response;

  private:
    static ZMQSocket* pinstance;
    thread* m_monitoring_thread_ptr;
    thread* m_responder_thread_ptr;
    int m_port = 5555;
    void* m_context_ptr = nullptr;
    void* m_responder_ptr = nullptr;
    void* m_monitor_ptr = nullptr;

    string m_responder_endpoint = "ipc://webcore";
    string m_monitor_endpoint = "inproc://webcore-monitor";
    string m_request_backup_dir = "/tmp/";

    bool m_is_responder_init = false;
    bool m_is_monitor_init = false;

    zmq_msg_t m_monitoring_msg;
    zmq_msg_t m_responder_msg;

    ZMQSocket();                                                     // Inicialización del entorno de comunicación con PHP.
    bool init_responder();                                           // Lanza el thread de escucha PHP.
    bool init_monitor();                                             // Lanza el thread de monitorización.
    void responder_method();                                         // Thread de escucha PHP.
    void monitoring_method();                                        // Thread de monitorización del socket PHP.
    bool parseMsg (zmq_msg_t& zmq_msg, string& zmq_msg_string);      // Convierte un mensaje zmq_msg_t a string si hay suficiente memoria.
    string jsonResponse (const ZMQSocket::Response& response,
                         string& response_status);                                // Convierte una string en formato JSON de respuesta para PHP.
    string event_str (const uint32_t& event_code);                   // Devuelve strings amigables a partir de códigos de eventos de 0MQ.
    void saveRequest (const string& json, string& id);               // Guarda la petición en un archivo con su respectivo id.
};

#endif /* WEBCORE_SRC_PHPSOCKET_HPP_ */

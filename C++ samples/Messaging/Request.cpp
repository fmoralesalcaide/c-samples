#include "Request.hpp"

Request::Request (Request::Type type, size_t machine_id)
{
    m_type = type;
    m_machine_id = machine_id;
}

Request::~Request()
{
    if (m_thread != nullptr)
    {
        m_thread->join();
        delete (m_thread);
    }

    ldebug ("Destruyendo request de máquina %d (%d)", m_machine_id, this);
}


const Request::Type&
Request::getType () const
{
    return m_type;
}

const bool& Request::isFinish() const
{
    return m_is_finish;
}

const size_t& Request::getMachineId() const
{
    return m_machine_id;
}

bool
Request::init ()
{
    if (m_is_init)
    {
        return true;
    }

    try
    {
        ldebug ("Lanzando thread_method en %d (%d)", m_machine_id, this);
        m_thread = new thread (&Request::thread_method, this);
    }
    catch (std::system_error& e)
    {
        lerror ("Error inciando el thread para la maquina %d", m_machine_id);
        return false;
    }

    m_is_init = true;
    return true;
}

void
Request::thread_method()
{
    ldebug ("Thread_method sin especificar %d completado (%d)", m_machine_id, this);
    m_is_finish = true;
}

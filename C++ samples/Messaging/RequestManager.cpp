#include "RequestManager.hpp"
#include <chrono>

RequestManager* RequestManager::pinstance = nullptr;

RequestManager&
RequestManager::Instance ()
{
    if (pinstance == nullptr)
        pinstance = new RequestManager ();

    return *pinstance;
}

RequestManager::RequestManager()
{
    linfo ("%s: Entry", __PRETTY_FUNCTION__);

    try
    {
        m_thread_ptr = new thread (&RequestManager::thread_method, this);
    }
    catch (std::system_error& e)
    {
        lerror ("%s: Cannot start thread", m_thread_ptr->get_id());
    }

    m_is_init = true;
}

RequestManager::~RequestManager()
{
    linfo ("%s: Entry", __PRETTY_FUNCTION__);

    if (m_thread_ptr != nullptr)
    {
        m_thread_ptr->join();
        delete (m_thread_ptr);
        linfo ("%s: Thread joined", __PRETTY_FUNCTION__);
    }
}

bool RequestManager::insert (Request* request_ptr)
{
    lock_guard<mutex> lock (m_pool_lock);
    int position = -1;

    for (size_t i = 0; i < m_request_pool.size() && position == -1; ++i)
        if (m_request_pool[i].first == request_ptr->getMachineId())
            position = i;

    if (position == -1)
    {
        m_request_pool.emplace_back (request_ptr->getMachineId(), request_ptr);
        request_ptr->init();
        ldebug ("New machine created with id %d (%d)", request_ptr->getMachineId(), request_ptr);
        ++m_current_requests;
        return true;
    }
    else if (m_request_pool[position].second->isFinish())
    {
        delete (m_request_pool[position].second);
        m_request_pool[position].second = request_ptr;
        request_ptr->init();
        ++m_served_requests;
        return false;
    }
    else
    {
        ++m_refused_requests;
        return false;
    }
}

void RequestManager::clean()
{
    lock_guard<mutex> lock (m_pool_lock);
    ldebug ("%s: Cleaning", __PRETTY_FUNCTION__);
    auto it = m_request_pool.begin();

    while (it != m_request_pool.end())
    {
        if (it->second->isFinish())
        {
            ldebug ("Deleting finished request from machine_id %d (%d)", it->first, it->second);
            delete (it->second);
            it = m_request_pool.erase (it);
            ++m_served_requests;
            --m_current_requests;
        }
        else
        {
            ldebug ("Starting request from machine_id %d (%d)", it->first, it->second);
            it->second->init();
            ++it;
        }
    }
}

void RequestManager::thread_method()
{
    linfo ("%s: Entry", __PRETTY_FUNCTION__);
    chrono::microseconds sleep (m_sleep_micro);

    while (true)
    {
        this_thread::sleep_for (sleep);
        clean();
    }

    linfo ("%s: Leave", __PRETTY_FUNCTION__);
}

const long int& RequestManager::getCurrentRequests()
{
    lock_guard<mutex> lock (m_pool_lock);
    return m_current_requests;
}

const long int& RequestManager::getServedRequests()
{
    lock_guard<mutex> lock (m_pool_lock);
    return m_served_requests;
}

const long int& RequestManager::getRefusedRequests()
{
    lock_guard<mutex> lock (m_pool_lock);
    return m_refused_requests;
}

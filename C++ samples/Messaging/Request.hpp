#pragma once

#include <thread>
#include <LogsManager.hpp>
#include <jsoncpp/json/reader.h>
#include <vector>
#include <set>
#include <chrono>
#include <ctime>

using namespace std;

class Request
{
  public:


    typedef enum
    {
      // Source: web
      _STOCK_COMMIT_ADD_ = 0,      // Insert new re-charge.
      _STOCK_COMMIT_EDIT_ = 1,     // Edit an existing re-charge.
      _STOCK_COMMIT_REMOVE_ = 2,   // Delete an existing re-charge.
      _STOCK_AUDIT_ADD_ = 3,       // Insert new audit.
      _STOCK_AUDIT_EDIT_ = 4,      // Edit an existing audit.
      _STOCK_AUDIT_REMOVE_ = 5,    // Delete an existing audit.
      _MACHINE_COMMIT_ADD_ = 6,    // Insert a new cigarette machine configuration.
      _MACHINE_COMMIT_EDIT_ = 7,   // Edit an existing configuration.
      _MACHINE_COMMIT_REMOVE_ = 8, // Delete an existing configuration.
      _RECOMPUTE_ = 9,             // Re-compute the accounting given a starting date.

      // Source: embedded (Raspberry Pi)
      _ACCOUNTING_TICKET_INSERT_ = 10,  // Insert an accounting ticket (only insertion).
      _ACCOUNTING_TICKET_PROCESS_ = 11, // Process sales of an accounting ticket already inserted (10->).
      _CHECK_PRICES_ = 12,              // Process prices of an accounting ticket already inserted (10->).
      _CHECK_THRESHOLDS_ = 15,          // Process stock thresholds of each status (10->).
      _ALERT_TICKET_ADD_ = 13,          // Process an alert ticket.
      _DOOR_TICKET_ADD_ = 14,           // Process a door opening ticket.

    } Type;

    Request (Request::Type type, size_t machine_id) ;
    virtual ~Request();

    const bool& isFinish() const;
    const Request::Type& getType () const;
    const size_t& getMachineId() const;

    bool init ();

  private:

    thread *m_thread = nullptr;
    bool m_is_finish = false;
    bool m_is_init = false;
    Request::Type m_type;
    size_t m_machine_id;

    virtual void thread_method ();

};


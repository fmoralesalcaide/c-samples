#pragma once

#include <thread>
#include <mutex>
#include "Request.hpp"

using namespace std;

class RequestManager
{
  public:
    /*! Constructor */
    static RequestManager& Instance ();
    bool insert (Request* request_ptr);
    virtual ~RequestManager();
    const long int& getCurrentRequests();
    const long int& getServedRequests();
    const long int& getRefusedRequests();

  private:
    static RequestManager* pinstance;

    thread* m_thread_ptr = nullptr;
    vector< pair<size_t, Request*> > m_request_pool;
    mutex m_pool_lock;
    long int m_served_requests  = 0;
    long int m_current_requests  = 0;
    long int m_refused_requests = 0;

    bool m_is_init = false;
    size_t m_sleep_micro = 100000; // 0.1 seconds

    RequestManager();
    void thread_method();
    void clean();
};

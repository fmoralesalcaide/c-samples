#include "../Messaging/ZMQSocket.hpp"

#include "RequestManager.hpp"
#include "RequestParser.hpp"
#include <LogsManager.hpp>
#include <cerrno>
#include <cstdlib>
#include <string>
#include <iostream>
#include <functional>
#include <sstream>
#include <jsoncpp/json/writer.h>

ZMQSocket* ZMQSocket::pinstance = nullptr;

ZMQSocket&
ZMQSocket::Instance ()
{
    if (pinstance == nullptr)
        pinstance = new ZMQSocket ();

    return *pinstance;
}

ZMQSocket::ZMQSocket()
{
    linfo ("%s: Entry", __PRETTY_FUNCTION__);
    m_context_ptr = zmq_ctx_new ();

    if (!m_context_ptr)
        lfatal ("Could not create ZeroMQ context: %s ", strerror (errno));

    m_responder_ptr = zmq_socket (m_context_ptr, ZMQ_REP);

    if (!m_responder_ptr)
        lfatal ("Could not create ZeroMQ socketMQ: %s ", strerror (errno));

    string endpoint = m_responder_endpoint + ":" + to_string (m_port);
    int rc = zmq_bind (m_responder_ptr, endpoint.c_str());

    if (rc)
        lfatal ("Could not create ZeroMQ socket: %s ", strerror (errno));

    rc = zmq_socket_monitor (m_responder_ptr, m_monitor_endpoint.c_str(), ZMQ_EVENT_ALL);

    if (rc)
        lfatal ("Could not monitor ZeroMQ socket: %s ", strerror (errno));

    rc = zmq_msg_init (&m_monitoring_msg);

    if (rc != 0)
    {
        lfatal ("Could not start monitoring message of ZeroMQ socket: %s ", strerror (errno));
    }

    rc = zmq_msg_init (&m_responder_msg);

    if (rc != 0)
    {
        lfatal ("Could not start web request container message: %s ", strerror (errno));
    }

    init_responder();
    init_monitor();
}

ZMQSocket::~ZMQSocket()
{
    linfo ("%s: Entry", __PRETTY_FUNCTION__);

    if (m_monitoring_thread_ptr != nullptr)
    {
        m_monitoring_thread_ptr->join();
        delete (m_monitoring_thread_ptr);
        linfo ("%s: Monitoring thread joined", __PRETTY_FUNCTION__);
    }

    if (m_responder_thread_ptr != nullptr)
    {
        m_responder_thread_ptr->join();
        delete (m_responder_thread_ptr);
        linfo ("%s: Listening thread joined", __PRETTY_FUNCTION__);
    }

    zmq_msg_close (&m_monitoring_msg);
    zmq_msg_close (&m_responder_msg);
    zmq_close (m_responder_ptr);
    zmq_close (m_monitor_ptr);
    zmq_ctx_term (m_context_ptr);
}

bool ZMQSocket::init_responder()
{
    if (m_is_responder_init)
    {
        return true;
    }

    try
    {
        m_responder_thread_ptr = new thread (&ZMQSocket::responder_method, this);
    }
    catch (std::system_error& e)
    {
        lerror ("Error initializing messaging thread with Apache httpd.");
        return false;
    }

    m_is_responder_init = true;
    return true;
}

bool ZMQSocket::init_monitor()
{
    if (m_is_monitor_init)
    {
        return true;
    }

    try
    {
        //ldebug ("Launching thread_method en %d (%d)", m_machine_id, this);
        m_monitoring_thread_ptr = new thread (&ZMQSocket::monitoring_method, this);
    }
    catch (std::system_error& e)
    {
        lerror ("Error initializing monitoring thread.");
        return false;
    }

    m_is_monitor_init = true;
    return true;
}

void ZMQSocket::responder_method()
{
    linfo ("%s: Entry", __PRETTY_FUNCTION__);
    RequestManager &request_manager = RequestManager::Instance();

    while (true)
    {
        if (zmq_msg_recv (&m_responder_msg, m_responder_ptr, 0) == -1)
        {
            lerror ("%s: zmq_msg_receive - %s", __PRETTY_FUNCTION__, strerror (errno));
            continue;
        }

        /* Parse message */
        string json_request = "";
        string json_response = "";
        string error_info = "";
        string response_status = "";
        string request_hash = "";
        Request* request_ptr = nullptr;

        if (!parseMsg (m_responder_msg, json_request))
        {
            json_response = jsonResponse (_WRONG_SIZE_, response_status);
        }
        else if (!RequestParser::Parse (json_request, request_ptr, error_info) || !request_ptr)
        {
            json_response = jsonResponse (_WRONG_FORMAT_, response_status);
        }
        else if (!request_manager.insert (request_ptr))
        {
            json_response = jsonResponse (_BUSY_, response_status);
        }
        else
        {
            json_response = jsonResponse (_SUCCESS_, response_status);
        }

        /* Send response */
        int rc = zmq_send (m_responder_ptr, json_response.c_str(), json_response.size(), 0);

        if (rc == -1)
        {
            saveRequest (json_request, request_hash);
            linfo ("%s: Could not reply the following request: '%s' (%s)", __PRETTY_FUNCTION__, strerror (errno), request_hash.c_str());
        }
        else if (response_status != "SUCCESS")
        {
            saveRequest (json_request, request_hash);
            linfo ("%s: Served requrest with status '%s' (%s)", __PRETTY_FUNCTION__, response_status.c_str(), request_hash.c_str());
        }
        else
            linfo ("%s: Served request with status 'SUCCESS'", __PRETTY_FUNCTION__);
    }

    linfo ("%s: Exit", __PRETTY_FUNCTION__);
}

void ZMQSocket::monitoring_method()
{
    linfo ("%s: Entry", __PRETTY_FUNCTION__);
    m_monitor_ptr = zmq_socket (m_context_ptr, ZMQ_PAIR);

    if (!m_monitor_ptr)
        lerror ("%s: Could not create ZeroMQ socket", __PRETTY_FUNCTION__);

    int rc = zmq_connect (m_monitor_ptr, m_monitor_endpoint.c_str());

    if (rc)
        lerror ("%s: Could not connect with %s", __PRETTY_FUNCTION__, m_monitor_endpoint.c_str());

    ldebug ("%s: Iteration", __PRETTY_FUNCTION__);

    while (true)
    {
        ldebug ("%s: Listening requests", __PRETTY_FUNCTION__);

        //  First frame in message contains event number and value
        if (zmq_msg_recv (&m_monitoring_msg, m_monitor_ptr, 0) == -1)
        {
            lerror ("%s: zmq_msg_receive - %s", __PRETTY_FUNCTION__, strerror (errno));
            continue;
        }

        if (!zmq_msg_more (&m_monitoring_msg))
        {
            lerror ("%s: zmq_msg_more - Expected one extra packet in the message", __PRETTY_FUNCTION__);
            continue;
        }

        size_t frame_size = zmq_msg_size (&m_monitoring_msg);

        if (frame_size != 6)
        {
            lerror ("%s: zmq_msg_size - First packet must be 6 bytes long", __PRETTY_FUNCTION__);
            continue;
        }

        uint16_t* it = (uint16_t*) zmq_msg_data (&m_monitoring_msg);
        uint32_t evt_number = *it;
        ++it;
        uint32_t evt_data =   * ( (uint32_t*) it);

        if (zmq_msg_recv (&m_monitoring_msg, m_monitor_ptr, 0) == -1)
        {
            lerror ("%s: zmq_msg_recv - %s", __PRETTY_FUNCTION__, strerror (errno));
            continue;
        }

        if (zmq_msg_more (&m_monitoring_msg))
        {
            lerror ("%s: zmq_msg_more - Received extra packets in the message.", __PRETTY_FUNCTION__);
            continue;
        }

        string evt_endpoint = "";

        if (!parseMsg (m_monitoring_msg, evt_endpoint))
            continue;

        linfo ("%s: Event %s @ '%s' (%d)", __PRETTY_FUNCTION__, event_str (evt_number).c_str(), evt_endpoint.c_str(), evt_data);
    }

    linfo ("%s: Exit", __PRETTY_FUNCTION__);
}

bool ZMQSocket::parseMsg (zmq_msg_t& zmq_msg, string& zmq_msg_string)
{
    zmq_msg_string.clear();
    uint8_t *data = (uint8_t *) zmq_msg_data (&zmq_msg);
    size_t size = zmq_msg_size (&zmq_msg);
    char* address = (char *) malloc (size);

    if (address == nullptr)
    {
        lerror ("%s: Could not allocate memory to store the message.", __PRETTY_FUNCTION__);
        return false;
    }

    memcpy (address, data, size);

    for (size_t i = 0; i < size; ++i)
        zmq_msg_string.push_back (address[i]);

    free (address);
    return true;
}

string ZMQSocket::event_str (const uint32_t& event_code)
{
    switch (event_code)
    {
    case _ZMQ_EVENT_CONNECTED_:
        return "_ZMQ_EVENT_CONNECTED_";

    case _ZMQ_EVENT_CONNECT_DELAYED_:
        return "_ZMQ_EVENT_CONNECT_DELAYED_";

    case _ZMQ_EVENT_CONNECT_RETRIED_:
        return "_ZMQ_EVENT_CONNECT_RETRIED_";

    case _ZMQ_EVENT_LISTENING_:
        return "_ZMQ_EVENT_LISTENING_";

    case _ZMQ_EVENT_BIND_FAILED_:
        return "_ZMQ_EVENT_BIND_FAILED_";

    case _ZMQ_EVENT_ACCEPTED_:
        return "_ZMQ_EVENT_ACCEPTED_";

    case _ZMQ_EVENT_ACCEPT_FAILED_:
        return "_ZMQ_EVENT_ACCEPT_FAILED_";

    case _ZMQ_EVENT_CLOSED_:
        return "_ZMQ_EVENT_CLOSED_";

    case _ZMQ_EVENT_CLOSE_FAILED_:
        return "_ZMQ_EVENT_CLOSE_FAILED_";

    case _ZMQ_EVENT_DISCONNECTED_:
        return "_ZMQ_EVENT_DISCONNECTED_";

    case _ZMQ_EVENT_MONITOR_STOPPED_:
        return "_ZMQ_EVENT_MONITOR_STOPPED_";

    default:
        return to_string (event_code);
    }
}

string ZMQSocket::jsonResponse (const ZMQSocket::Response& response, string& response_status)
{
    Json::Value root;

    switch (response)
    {
    case _SUCCESS_:
        response_status = "SUCCESS";
        root["status"] = response_status;
        break;

    case _BUSY_:
        response_status = "BUSY";
        root["status"] = response_status;
        break;

    case _WRONG_FORMAT_:
        response_status = "WRONG_FORMAT";
        root["status"] = response_status;
        break;

    case _WRONG_SIZE_:
        response_status = "WRONG_SIZE";
        root["status"] = response_status;
        break;

    default:
        response_status = "NOT_SPECIFIED";
        root["status"] = response_status;
    }

    Json::StyledWriter writer;
    return writer.write (root);
}

void ZMQSocket::saveRequest (const string& json, string& id)
{
    /* Compute unique id */
    std::hash<string> string_hash;
    std::stringstream ss;
    ss << std::uppercase << std::hex << string_hash (json);
    id = ss.str().substr (0, 8);
    /* Save data to file */
    std::ofstream ofs (m_request_backup_dir + id + ".json", std::ofstream::out);
    ofs << json;
    ofs.close();
}

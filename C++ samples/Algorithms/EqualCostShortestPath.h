#pragma once

#include "Lib/RoutingAlgorithms/RoutingAlgorithm.h"

namespace Lib
{
    namespace RoutingAlgorithms
    {
        /*! Equal cost shortest path algorithm (ECSP). This routing
         * algorithm computes the equal cost shortest paths between a source node
         * and a set of destination nodes. This is, more than a shortest path might
         * be returned between a pair source-destination if they have the same
         * routing cost.
         *
         * A heap-based priority queue variant of Dijkstra's algorithm is implemented.
         */
        class EqualCostShortestPath : public RoutingAlgorithm
        {
            public:

                explicit EqualCostShortestPath(LN::Network* network, const uint32_t& rootNodeId, RoutingConstraints* constraints);
                virtual ~EqualCostShortestPath();

            private:

                /*!
                    In this method, the Dijkstra's algorithm between the source and the given destination nodes is executed.
                    \param dstIds The set of destination nodes.
                */
                void doShortestPaths(const LN::NodeIdVector& dstIds);

                /*!
                    After execution, the set of equal cost shortest paths are explicitly built by rebuilding the
                    shortest path information that is implicitly stored in the predecessor list in all the nodes.
                    \param dstIds The set of destination nodes.
                */
                void computeEqualCostShortestPaths(const LN::NodeIdVector& dstIds);

                /*!
                    Returns the equal cost shortest paths found between the source and the given destination node.
                    \param dstId The destination node.
                    \return The equal-cost shortest paths to the specified destination node.
                */
                LinkVectorVector getEqualCostShortestPaths(const uint32_t& dstId);

                /*!
                    Three different actions are applied in this method:
                    - The Dijkstra's algorithm is executed between the source and every destination node (see doShortestPaths()).
                    - For each destination node, the set of equal cost shortest paths is computed (see getEqualCostShortestPaths()).
                    - The obtained routes are finally returned (as vector of hop lists - i.e., LinkVector).
                    \param srcIds The set of source node identifiers.
                    \param dstIds The set of destination node identifiers.
                    \return The equal-cost shortest paths from the source nodes to the specified destination nodes.
                */
                LinkVectorVector computeRoutes(const LN::NodeIdVector& srcIds, const LN::NodeIdVector& dstIds);
        };
    }
}

namespace LRA = Lib::RoutingAlgorithms;

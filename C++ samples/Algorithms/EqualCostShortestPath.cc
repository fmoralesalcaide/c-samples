#include "Lib/RoutingAlgorithms/Algorithms/EqualCostShortestPath.h"
#include "Lib/RoutingAlgorithms/VolatileNode.h"
#include "Lib/RoutingAlgorithms/VolatileLink.h"

namespace Lib
{
    namespace RoutingAlgorithms
    {

        EqualCostShortestPath::EqualCostShortestPath(LN::Network* network, const uint32_t& rootNodeId, RoutingConstraints* constraints)
        : RoutingAlgorithm(network, rootNodeId, constraints)
        {}

        EqualCostShortestPath::~EqualCostShortestPath()
        {}

        void EqualCostShortestPath::doShortestPaths(const LN::NodeIdVector& dstIds)
        {
            typedef std::pair<uint64_t, uint32_t> DistanceToNode;
            struct DistanceComparator
            {
                    bool operator()(const DistanceToNode& lhs, const DistanceToNode& rhs) const
                    {
                        return lhs.first < rhs.first;
                    }
            };
            std::priority_queue<DistanceToNode, std::vector<DistanceToNode>, DistanceComparator> Q;

            if(!constraints->verifyNode(root->getOriginalNode()))
                return;

            root->distanceFromRoot = 0;
            Q.push(DistanceToNode(0, root->getId()));

            while(!Q.empty())
            {
                DistanceToNode v = Q.top();
                Q.pop();
                const uint64_t& cost = v.first;

                VolatileNode* currentNode = (VolatileNode*) nodeGet(v.second);
                const LN::LinkVector& outEdges = currentNode->getOutputLinks();

                for(LN::LinkVector::const_iterator it = outEdges.begin(); it != outEdges.end(); ++it)
                {
                    VolatileLink* link = (VolatileLink*) *it;
                    VolatileNode* adjacentNode = (VolatileNode*) link->getEndPointB().first;

                    uint64_t distance = cost + link->getVolatileMetric(constraints) + adjacentNode->getVolatileMetric(constraints);

                    if(distance < MAX_UINT32)
                    {
                        if(distance < adjacentNode->distanceFromRoot)
                        {
                            adjacentNode->vPredecessors = { link };
                            adjacentNode->sPredecessors = { link->getId() };
                            adjacentNode->distanceFromRoot = distance;
                            Q.push(DistanceToNode(distance, adjacentNode->getId()));
                        }
                        else if(distance == adjacentNode->distanceFromRoot && !adjacentNode->sPredecessors.count(link->getId()))
                        {
                            adjacentNode->vPredecessors.push_back(link);
                            adjacentNode->sPredecessors.insert(link->getId());
                            Q.push(DistanceToNode(distance, adjacentNode->getId()));
                        }
                    }

                }
            }

            computeEqualCostShortestPaths(dstIds);
        }

        void EqualCostShortestPath::computeEqualCostShortestPaths(const LN::NodeIdVector& dstIds)
        {
            std::unordered_set<uint32_t> destinationSet;
            for(const uint32_t& dstId : dstIds)
                destinationSet.insert(dstId);

            struct VolatileComparator
            {
                    bool operator()(const LN::Node* lhs, const LN::Node* rhs)
                    {
                        return ((VolatileNode*) lhs)->distanceFromRoot < ((VolatileNode*) rhs)->distanceFromRoot;
                    }
            };
            std::sort(vNodes.begin(), vNodes.end(), VolatileComparator());

            for(LN::Node* baseNode : vNodes)
            {
                VolatileNode* node = (VolatileNode*) baseNode;
                node->routeList.clear();

                if(node == root)
                    continue;

                for(LN::Link* baseLink : node->vPredecessors)
                {
                    VolatileLink* linkToDescendant = (VolatileLink*) baseLink;
                    VolatileNode* pred = (VolatileNode*) linkToDescendant->getEndPointA().first;

                    if(pred == root)
                    {
                        LN::LinkVector oneHopRoute = { linkToDescendant->getOriginalLink() };
                        node->routeList.push_back(oneHopRoute);
                    }
                    else
                    {
                        for(LN::LinkVector route : pred->routeList)
                        {
                            route.push_back(linkToDescendant->getOriginalLink());
                            node->routeList.push_back(route);
                        }
                    }
                }

                if(destinationSet.count(node->getId()))
                {
                    destinationSet.erase(node->getId());
                    if(destinationSet.empty())
                        return;
                }
            }
        }

        LinkVectorVector EqualCostShortestPath::getEqualCostShortestPaths(const uint32_t& dstId)
        {
            return nodeExists(dstId) ? ((VolatileNode*) nodeGet(dstId))->routeList : LinkVectorVector(0);
        }

        LinkVectorVector EqualCostShortestPath::computeRoutes(const LN::NodeIdVector& srcIds, const LN::NodeIdVector& dstIds)
        {
            (void) srcIds;
            doShortestPaths(dstIds);
            LinkVectorVector routes;

            for(uint32_t dstId : dstIds)
            {
                LinkVectorVector shortestPaths = getEqualCostShortestPaths(dstId);
                routes.insert(routes.end(), shortestPaths.begin(), shortestPaths.end());
            }

            return routes;
        }
    }
}


#!/bin/bash

# Format files
FILES="*.cpp *.h"
astyle --style=allman --recursive --mode=c --lineend=linux $FILES

# Delete backup files
find . -type f -name '*.orig' -delete
